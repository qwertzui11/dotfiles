-- bootstrap lazy.nvim, LazyVim and your plugins
require("config.lazy")

-- support for the "cool" editor for nvim
require("neovide").setup()
-- c/c++ include guards using an uuid
require("insert-include-guards").setup()

vim.api.nvim_create_user_command("DapLoadLaunchJSON", function()
  -- improve DapLoadLaunchJSON to assiciate codelldb with rust, c and c++
  require("dap.ext.vscode").load_launchjs(nil, { codelldb = { "c", "cpp", "rust" } })
end, {})

vim.api.nvim_create_user_command("Terminal", function()
  vim.api.nvim_command("term fish")
  vim.cmd.startinsert()
end, {})

vim.api.nvim_create_user_command("TTerminal", function()
  vim.api.nvim_command("tabe | Terminal")
end, {})

vim.api.nvim_create_user_command("STerminal", function()
  vim.api.nvim_command("split | Terminal")
end, {})

vim.api.nvim_create_user_command("VTerminal", function()
  vim.api.nvim_command("vsplit | Terminal")
end, {})
