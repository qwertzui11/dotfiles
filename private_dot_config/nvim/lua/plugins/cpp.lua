local utils = require("utils")

return {
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, { "cuda", "proto" })
    end,
  },
  -- most stuff set in `extras/lang.clangd`, https://www.lazyvim.org/extras/lang/clangd
  {
    "neovim/nvim-lspconfig",
    opts = {
      servers = {
        neocmake = {
          mason = utils.is_arch_amd64(),
        },
        clangd = {
          mason = utils.is_arch_amd64(),
          cmd = {
            "clangd",
            "--background-index",
            "--clang-tidy",
            "--header-insertion=never",
            "--completion-style=detailed",
            "--function-arg-placeholders",
            "--fallback-style=google",
          },
        },
      },
    },
  },
  {
    -- comes with `lang.cmake` - however does not add features I need
    "Civitasv/cmake-tools.nvim",
    enabled = false,
  },
}
