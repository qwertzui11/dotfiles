# no greeting
set fish_greeting

# docker
## docker run
alias ,dr 'docker run'
alias ,drr 'docker run --rm'
alias ,drrit 'docker run --rm --interactive --tty'
## docker image
alias ,di 'docker image'
## docker ps
alias ,dp 'docker ps'
alias ,dpa 'docker ps --all'

# chezmoi
alias ,c chezmoi
alias ,cs 'chezmoi status'
alias ,cg 'chezmoi git --'

# llmchat
alias llmchatml 'llm chat -m mistral-large'
alias llmml 'llm -m mistral-large'

# mullvad
function ,mcdefr --wraps mullvad --description 'Connect Mullvad to de-fr'
    mullvad relay set location de fra
    mullvad reconnect --wait
    mullvad connect --wait
end
function ,mcatvie --wraps mullvad --description 'Connect Mullvad to at-vie'
    mullvad relay set location at vie
    mullvad reconnect --wait
    mullvad connect --wait
end
function ,mcsegot --wraps mullvad --description 'Connect Mullvad to se-got'
    mullvad relay set location se got
    mullvad reconnect --wait
    mullvad connect --wait
end
function ,mcusmia --wraps mullvad --description 'Connect Mullvad to us-mia'
    mullvad relay set location us mia
    mullvad reconnect --wait
    mullvad connect --wait
end
alias ,ms 'mullvad status'
alias ,md 'mullvad disconnect --wait'

# neovim
function e --wraps=nvim --description 'Start neovim'
    if set -q NVIM
        # when terminal inside neovim open neovim in-place
        if test (count $argv) -gt 0
            set --local files (realpath $argv)
            nvim --server $NVIM --remote $files
        else
            echo "No arguments provided. Ignoring"
        end
    else
        nvim $argv
    end
end

# `ls` alteritive
alias ls eza
alias lln 'll -snew'
alias lla 'll -a'

# nice prompt
starship init fish | source
