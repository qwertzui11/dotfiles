local M = {}

function M.setup()
  if vim.g.neovide then
    vim.g.neovide_fullscreen = false
    -- for scroll animation to work, one has to start `neovide` with 
    -- `--multigrid`
    -- https://neovide.dev/faq.html#how-to-enable-scrolling-animations-and-transparency
    vim.g.neovide_scroll_animation_length = 0.2
    vim.g.neovide_cursor_vfx_mode = 'wireframe'
    vim.opt.guifont = { 'VictorMono Nerd Font', ':h12' }
  end
end

return M
