return {
  "epwalsh/obsidian.nvim",
  version = "*", -- recommended, use latest release instead of latest commit
  -- currently not maintained. And no blink.nvim support.
  -- Community is currently deciding on a fork, let's wait and see.
  enabled = false,
  -- lazy = true,
  -- ft = "markdown",
  -- only load obsidian.nvim for markdown files in notes:
  event = {
    "BufReadPre " .. vim.fn.expand("~") .. "/notes/*.md",
    "BufNewFile " .. vim.fn.expand("~") .. "/notes/*.md",
  },
  dependencies = {
    -- Required.
    "nvim-lua/plenary.nvim",
    "ibhagwan/fzf-lua",
  },
  opts = {
    workspaces = {
      {
        name = "personal",
        path = "~/notes/personal",
      },
    },
    picker = {
      name = "fzf-lua",
    },
  },
}
