return {
  {
    "folke/snacks.nvim",
    opts = function(_, opts)
      local logo = [[
              _                                                
             | |                                               
             | |===( )   //////                                
             |_|   |||  | o o|                                 
                    ||| ( c  )                  ____           
                     ||| \= /                  ||   \_         
                      ||||||                   ||     |        
                      ||||||                ...||__/|-"        
                      ||||||             __|________|__        
                        |||             |______________|       
                        |||             || ||      || ||       
                        |||             || ||      || ||       
------------------------|||-------------||-||------||-||-------
                        |__>            || ||      || ||       
      ]]
      opts.dashboard.preset.header = logo

      local terminal = { action = "<cmd>Terminal<cr>", desc = "Terminal", icon = "⌨ ", key = "t" }
      table.insert(opts.dashboard.preset.keys, 4, terminal)

      local project = {
        action = "<cmd>CdProject<cr>",
        desc = "Projects",
        icon = " ",
        key = "p",
      }
      table.insert(opts.dashboard.preset.keys, 5, project)

      return opts
    end,
  },
  {
    "LintaoAmons/cd-project.nvim",
    opts = {
      auto_register_project = true,
    },
    lazy = false,
    -- cmd = {
    --   "CdProject",
    -- },
    keys = {
      { "<leader>fp", "<cmd>CdProject<cr>", desc = "Change directory to a project" },
      { "<leader>fP", "<cmd>CdProjectTab<cr>", desc = "Change tab directory to a project" },
    },
  },
  {
    "LazyVim/LazyVim",
    opts = {
      -- colorscheme = "tokyonight",
      colorscheme = "catppuccin",
    },
  },
  {
    "folke/todo-comments.nvim",
    opts = {
      highlight = {
        -- pattern = [[.*<(KEYWORDS)\s*:?]],
      },
      search = {
        -- pattern = [[\b(KEYWORDS)\b]],
      },
    },
  },
  {
    "lambdalisue/suda.vim",
    lazy = true,
    cmd = { "SudaRead", "SudaWrite" },
  },
  {
    "earthly/earthly.vim",
    lazy = true,
    ft = "Earthfile",
  },
  {
    "isobit/vim-caddyfile",
    lazy = true,
    ft = "caddyfile",
  },
  {
    "sindrets/diffview.nvim",
    config = true,
  },
  {
    "RRethy/vim-illuminate",
    -- disable illuminate for large files. else the editor starts to hang
    opts = { large_file_cutoff = 10000 },
  },
  {
    "stevearc/oil.nvim",
    config = true,
    enabled = false,
  },
  {
    "nvim-neo-tree/neo-tree.nvim",
    enabled = true,
  },
}
